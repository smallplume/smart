package com.smt.common;

import java.util.List;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/14 13:58
 */
public class PageBean<T> {
    /**
     * 数据列表
     */
    private List<T> data;
    /**
     * 表中一共有多少条数据
     */
    private long total;
    /**
     * 每页有多少数据 默认10
     */
    private int pageSize = 10;
    /**
     * 当前页的页码
     */
    private int pageNum;
    /**
     * 该表一共有多少页
     */
    private int totalPage;
    /**
     * 需要展示的第一页的标号
     */
    private int startPage;
    /**
     * 需要展示的最后一页的标号
     */
    private int endPage;
    /**
     * 需要展示多少个页码
     */
    private int showPageCount = 10;

    public PageBean(int pageNum, int pageSize, long total, List<T> data) {
        this.pageNum = pageNum + 1;
        this.pageSize = pageSize;
        this.total = total;
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getTotalPage() {
        return (int) Math.ceil(total * 1.0 / pageSize);
    }

    public int getStartPage() {
        return getRange()[0];
    }

    public int getEndPage() {
        return getRange()[1];
    }

    public int getShowPageCount() {
        return showPageCount;
    }

    public void setShowPageCount(int showPageCount) {
        this.showPageCount = showPageCount;
    }

    public int[] getRange() {
        totalPage = getTotalPage();

        // 先要判断前面需要几个后面需要几个  奇数页和偶数页是不同的
        int pre = showPageCount % 2 == 0 ? showPageCount / 2 : showPageCount / 2;
        int suf = showPageCount % 2 == 0 ? showPageCount / 2 - 1 : showPageCount / 2;

        // 前面不够   后面够的情况
        if (!(pageNum > pre) && (pageNum + suf) <= totalPage) {
            this.startPage = 1;
            // 计算前面还缺多少页  缺的看看能补到后面吗
            int preLackNum = pre - (pageNum - 1);
            this.endPage = (pageNum + preLackNum + suf) > totalPage ? totalPage : pageNum + preLackNum + suf;
            return new int[]{this.startPage, this.endPage};
        }
        // 前面不够  后面不够的情况
        if (!(pageNum > pre) && !((pageNum + suf) <= totalPage)) {
            this.startPage = 1;
            this.endPage = totalPage;
            return new int[]{this.startPage, this.endPage};
        }
        // 前面够 后面够的情况
        if (pageNum > pre && (pageNum + suf) <= totalPage) {
            this.startPage = pageNum - pre;
            this.endPage = pageNum + suf;
            return new int[]{this.startPage, this.endPage};
        }
        // 前面够 后面不够的情况
        if (pageNum > pre && !((pageNum + suf) <= totalPage)) {
            this.endPage = totalPage;
            // 先看下后面还差几页
            int sufLackNum = pageNum + suf - totalPage;
            // 看看前面还有没有空间可以补上
            this.startPage = (pageNum > (sufLackNum + pre)) ? pageNum - (sufLackNum + pre) : 1;
            return new int[]{this.startPage, this.endPage};
        }
        return new int[]{1, 1};
    }

}
