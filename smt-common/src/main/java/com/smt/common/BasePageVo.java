package com.smt.common;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/14 11:13
 */
public class BasePageVo<T> extends BaseVo {

    private int pageNum = 0;
    private int pageSize = 10;
    private String orderBy;
    private String order;

    public BasePageVo() {
    }

    public int getPageSize() {
        return this.pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrderBy() {
        return this.orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getOrder() {
        return this.order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getPageNum() {
        return this.pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum > 0 ? pageNum - 1 : pageNum;
    }

}
