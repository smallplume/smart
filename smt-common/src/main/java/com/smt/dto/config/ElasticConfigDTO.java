package com.smt.dto.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Objects;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/11 11:07
 */
@ConfigurationProperties(prefix = "elastic")
public class ElasticConfigDTO {
    /**
     * TCP 使用
     */
    private String nodes;
    /**
     * HTTP 使用
     */
    private String hosts;

    private String name;

    private Boolean enabled;

    /**
     * 传输方式：http、ctp
     */
    private String schema;

    private String logs;

    private String data;

    private Integer connectNum;

    private Integer connectPerRoute;

    public String getNodes() {
        return nodes;
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getLogs() {
        return logs;
    }

    public void setLogs(String logs) {
        this.logs = logs;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public String getHosts() {
        return hosts;
    }

    public void setHosts(String hosts) {
        this.hosts = hosts;
    }

    public Integer getConnectNum() {
        return connectNum;
    }

    public void setConnectNum(Integer connectNum) {
        this.connectNum = connectNum;
    }

    public Integer getConnectPerRoute() {
        return connectPerRoute;
    }

    public void setConnectPerRoute(Integer connectPerRoute) {
        this.connectPerRoute = connectPerRoute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ElasticConfigDTO that = (ElasticConfigDTO) o;
        return Objects.equals(nodes, that.nodes) &&
                Objects.equals(hosts, that.hosts) &&
                Objects.equals(name, that.name) &&
                Objects.equals(enabled, that.enabled) &&
                Objects.equals(schema, that.schema) &&
                Objects.equals(logs, that.logs) &&
                Objects.equals(data, that.data) &&
                Objects.equals(connectNum, that.connectNum) &&
                Objects.equals(connectPerRoute, that.connectPerRoute);
    }

    @Override
    public int hashCode() {

        return Objects.hash(nodes, hosts, name, enabled, schema, logs, data, connectNum, connectPerRoute);
    }

    @Override
    public String toString() {
        return "ElasticConfigDTO{" +
                "nodes='" + nodes + '\'' +
                ", hosts='" + hosts + '\'' +
                ", name='" + name + '\'' +
                ", enabled=" + enabled +
                ", schema='" + schema + '\'' +
                ", logs='" + logs + '\'' +
                ", data='" + data + '\'' +
                ", connectNum=" + connectNum +
                ", connectPerRoute=" + connectPerRoute +
                '}';
    }

}
