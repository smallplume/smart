package com.smt.dto.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author SmallPlume
 * @title: RedisConfig
 * @projectName smart
 * @description: @Data 失效，需要手动添加setter 和 getter
 * @date 2019/6/10 23:04
 */
@ConfigurationProperties(prefix = "redis")
public class RedisConfigDTO {
    /**
     * 主机
     */
    private String host;
    /**
     * 端口号
     */
    private Integer port;
    /**
     * 密码
     */
    private String password;
    /**
     * 超时
     */
    private Integer timeout = 1000;
    /**
     * 0-15 16个库 默认0
     */
    private Integer database = 0;
    /**
     * 最大连接数
     */
    private Integer maxActive = 8;
    /**
     * 默认-1 最大连接阻塞等待时间
     */
    private Integer maxWait = -1;
    /**
     * 最大空闲连接 默认8
     */
    private Integer maxIdle = 8;
    /**
     * 最小空闲连接
     */
    private Integer minIdle = 0;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public Integer getDatabase() {
        return database;
    }

    public void setDatabase(Integer database) {
        this.database = database;
    }

    public Integer getMaxActive() {
        return maxActive;
    }

    public void setMaxActive(Integer maxActive) {
        this.maxActive = maxActive;
    }

    public Integer getMaxWait() {
        return maxWait;
    }

    public void setMaxWait(Integer maxWait) {
        this.maxWait = maxWait;
    }

    public Integer getMaxIdle() {
        return maxIdle;
    }

    public void setMaxIdle(Integer maxIdle) {
        this.maxIdle = maxIdle;
    }

    public Integer getMinIdle() {
        return minIdle;
    }

    public void setMinIdle(Integer minIdle) {
        this.minIdle = minIdle;
    }

    @Override
    public String toString() {
        return "RedisConfigDTO{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", password='" + password + '\'' +
                ", timeout=" + timeout +
                ", database=" + database +
                ", maxActive=" + maxActive +
                ", maxWait=" + maxWait +
                ", maxIdle=" + maxIdle +
                ", minIdle=" + minIdle +
                '}';
    }

}
