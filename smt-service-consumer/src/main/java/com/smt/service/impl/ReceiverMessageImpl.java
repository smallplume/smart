package com.smt.service.impl;

import com.smt.service.ReceiverMessage;
import com.smt.support.ReceiveInputChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/12 15:49
 */
@Slf4j
@EnableBinding(ReceiveInputChannel.class)
public class ReceiverMessageImpl implements ReceiverMessage {

    @Override
    @StreamListener(target = ReceiveInputChannel.NOTE_RECEIVER)
    public void receiveNoteStr(Message<String> message) {
        String s = message.getPayload();
        log.info("[短信]消费者接收：" + s);
    }

    @Override
    @StreamListener(target = ReceiveInputChannel.EMAIL_RECEIVER)
    public void receiveEmailStr(Message<String> message) {
        String s = message.getPayload();
        log.info("[邮件]消费者接收：" + s);
    }

}
