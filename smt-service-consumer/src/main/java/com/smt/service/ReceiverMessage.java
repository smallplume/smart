package com.smt.service;

import org.springframework.messaging.Message;

/**
 * 消息消费接口
 *
 * @autor xiaoyu.fang
 * @date 2019/6/12 15:48
 */
public interface ReceiverMessage {

    void receiveNoteStr(Message<String> message);

    void receiveEmailStr(Message<String> message);

}
