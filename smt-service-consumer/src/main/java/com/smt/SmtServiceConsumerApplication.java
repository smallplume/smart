package com.smt;

import com.smt.dto.config.RabbitmqConfigDTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties({RabbitmqConfigDTO.class})
public class SmtServiceConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmtServiceConsumerApplication.class, args);
	}

}
