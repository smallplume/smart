package com.smt.support;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * 自定义消息消费通道
 *
 * @autor xiaoyu.fang
 * @date 2019/6/12 15:43
 */
public interface ReceiveInputChannel {

    String NOTE_RECEIVER = "noteReceiver";

    String EMAIL_RECEIVER = "emailReceiver";

    /**
     * 短信通道
     * @return
     */
    @Input(ReceiveInputChannel.NOTE_RECEIVER)
    SubscribableChannel noteReceiver();

    /**
     * 邮件通道
     * @return
     */
    @Input(ReceiveInputChannel.EMAIL_RECEIVER)
    SubscribableChannel emailReceiver();
}
