package com.smt;

import com.smt.dto.config.ElasticConfigDTO;
import com.smt.dto.config.RedisConfigDTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties({RedisConfigDTO.class, ElasticConfigDTO.class})
public class SmtServiceBusinessApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmtServiceBusinessApplication.class, args);
    }

}
