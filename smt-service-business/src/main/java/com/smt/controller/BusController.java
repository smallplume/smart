package com.smt.controller;

import com.smt.common.BaseVo;
import com.smt.entity.Student;
import com.smt.service.StudentService;
import com.smt.vo.StudentQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/18 11:38
 */
@RestController
@RequestMapping("/bus")
public class BusController {

    @Autowired
    private StudentService studentService;

    @PostMapping("/student/insert")
    public BaseVo insertStudent(@RequestBody @Validated(Student.Create.class) Student student) throws IOException {
        studentService.insert(student);
        return new BaseVo();
    }

    @PostMapping("/student/update")
    public BaseVo updateStudent(@RequestBody @Validated(Student.Update.class) Student student) throws IOException {
        studentService.update(student);
        return new BaseVo();
    }

    @GetMapping("/student/{id}/delete")
    public BaseVo deleteStudent(@PathVariable("id") String id) throws IOException {
        studentService.delete(id);
        return new BaseVo();
    }

    @GetMapping("/student/{id}/detail")
    public BaseVo findStudent(@PathVariable("id") String id) throws IOException {
        return new BaseVo(studentService.findOne(id));
    }

    @GetMapping("/student/query")
    public BaseVo queryStudent(@RequestParam(value = "name", required = false) String name
            , @RequestParam(value = "identity", required = false) String identity
            , @RequestParam(value = "address", required = false) String address) throws IOException {
        return new BaseVo(studentService.query(name, identity, address));
    }

    @PostMapping("/student/page")
    public BaseVo pageStudent(@RequestBody StudentQuery query) throws IOException {
        return new BaseVo(studentService.page(query));
    }

    @GetMapping("/student/joinquery")
    public BaseVo joinQuery() throws IOException {
        studentService.joinquery();
        return new BaseVo();
    }

}
