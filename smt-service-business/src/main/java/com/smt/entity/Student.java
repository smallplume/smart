package com.smt.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/17 11:27
 */
@Data
public class Student implements Serializable {
    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = {Update.class})
    private String id;
    /**
     * 学生名称
     */
    @NotNull(message = "名称不能为空", groups = {Create.class, Update.class})
    private String name;
    /**
     * 身份证
     */
    private String identity;
    /**
     * 联系地址
     */
    private String address;
    /**
     * 班级
     */
    private String classId;
    /**
     * 创建时间
     */
    private Date createAt;

    public interface Create {}

    public interface Update {}

}
