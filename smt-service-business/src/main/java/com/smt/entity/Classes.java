package com.smt.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/17 14:30
 */
@Data
public class Classes implements Serializable {
    /**
     * ID
     */
    private String id;
    /**
     * 名称
     */
    private String name;
    /**
     * 第几届，如：2019届
     */
    private String grade;
    /**
     * 创建时间
     */
    private Date createAt;

}
