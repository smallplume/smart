package com.smt.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/17 14:35
 */
@Data
public class Teacher implements Serializable {
    /**
     * ID
     */
    private String id;
    /**
     * 教师名称
     */
    private String name;
    /**
     * 性别
     */
    private Integer sex;
    /**
     * 入职时间
     */
    private Date hiredate;
    /**
     * 教导班级[数组]
     */
    private String classIds;

}
