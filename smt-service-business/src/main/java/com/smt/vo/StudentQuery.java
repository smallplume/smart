package com.smt.vo;

import com.smt.common.BasePageVo;
import com.smt.entity.Student;
import lombok.Data;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/18 15:02
 */
@Data
public class StudentQuery extends BasePageVo<Student> {

    private String name;

    private String identity;

    private String address;

}
