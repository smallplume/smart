package com.smt.service;

import com.smt.common.PageBean;
import com.smt.entity.Student;
import com.smt.vo.StudentQuery;

import java.io.IOException;
import java.util.List;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/17 16:16
 */
public interface StudentService {

    void insert(Student student) throws IOException;

    void update(Student student) throws IOException;

    void delete(String id) throws IOException;

    Student findOne(String id) throws IOException;

    List<Student> query(String name, String identity, String address) throws IOException;

    PageBean<Student> page(StudentQuery query) throws IOException;

    void joinquery() throws IOException;

    void createIndex(String index) throws IOException;

    boolean existsIndex(String index) throws IOException;

}
