package com.smt.repository;

import java.io.Serializable;
import java.util.List;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/13 11:16
 */
public interface IBookRepository<T, ID extends Serializable> {

    List<T> queryByBook(T t);

}
