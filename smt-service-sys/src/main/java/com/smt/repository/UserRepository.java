package com.smt.repository;

import com.smt.entity.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

/**
 * Created by fxiao
 * on 2019/4/24
 */
@Component
public interface UserRepository extends ElasticsearchRepository<User, String> {

}
