package com.smt.repository;

import com.smt.entity.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/13 10:27
 */
@Repository
public interface BookRepository extends ElasticsearchRepository<Book, String>, IBookRepository<Book, String> {

}
