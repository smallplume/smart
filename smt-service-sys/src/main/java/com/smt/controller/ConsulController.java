package com.smt.controller;

import com.smt.common.BaseVo;
import com.smt.dto.config.ElasticConfigDTO;
import com.smt.dto.config.RedisConfigDTO;
import com.smt.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/11 10:13
 */
@RestController
@RequestMapping("/config")
public class ConsulController {

    @Autowired
    private RedisConfigDTO redisConfigDTO;

    @Autowired
    private ElasticConfigDTO elasticConfigDTO;

    @Autowired
    private ConfigService configService;

    @GetMapping("/redis")
    public BaseVo getRedisConfig() {
        return new BaseVo(redisConfigDTO.toString());
    }

    @GetMapping("/elastic")
    public BaseVo getElasticConfig() {
        return new BaseVo(elasticConfigDTO.toString());
    }

    @GetMapping("/setCache")
    public BaseVo setCache(@RequestParam(value = "key", required = true) String key, @RequestParam(value = "value", required = true) String value) {
        configService.setCache(key, value);
        return new BaseVo();
    }

    @GetMapping("/getCache")
    public BaseVo getCache(@RequestParam(value = "key", required = false) String key) {
        return new BaseVo(configService.getCache(key));
    }

}
