package com.smt.controller;

import com.smt.common.BaseVo;
import com.smt.service.SendMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/12 15:56
 */
@RestController
@RequestMapping("/stream")
public class StreamController {

    @Autowired
    private SendMessage sendMessage;

    @GetMapping("/note/send")
    public BaseVo sendNoteStr(@RequestParam(value = "msg") String msg) {
        sendMessage.sendNoteStr(msg);
        return new BaseVo();
    }

    @GetMapping("/email/send")
    public BaseVo sendEmailStr(@RequestParam(value = "msg") String msg) {
        sendMessage.sendEmailStr(msg);
        return new BaseVo();
    }

}
