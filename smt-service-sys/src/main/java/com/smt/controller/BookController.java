package com.smt.controller;

import com.smt.common.BaseVo;
import com.smt.common.PageBean;
import com.smt.entity.Book;
import com.smt.service.BookService;
import com.smt.vo.BookQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/13 11:44
 */
@RestController
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    @PostMapping("/insert")
    public BaseVo insert(@RequestBody @Validated(Book.Create.class) Book book) {
        bookService.insert(book);
        return new BaseVo();
    }

    @PostMapping("/update")
    public BaseVo update(@RequestBody @Validated(Book.Update.class) Book book) {
        bookService.update(book);
        return new BaseVo();
    }

    @GetMapping("/{id}/detail")
    public BaseVo find(@PathVariable(value = "id") String id) {
        Book book = bookService.find(id);
        return new BaseVo(book);
    }

    @PostMapping("/query")
    public BaseVo query(@RequestParam(value = "name", required = false) String name
            , @RequestParam(value = "auther", required = false) String auther) {
        List<Book> list = bookService.query(name, auther);
        return new BaseVo(list);
    }

    @PostMapping("/page")
    public BaseVo page(@RequestBody BookQuery bookQuery) {
        PageBean<Book> bookPage = bookService.page(bookQuery);
        return new BaseVo(bookPage);
    }

}
