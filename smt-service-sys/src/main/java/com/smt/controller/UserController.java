package com.smt.controller;

import com.smt.common.BaseVo;
import com.smt.entity.User;
import com.smt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/11 11:27
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/insert")
    public BaseVo insertUser(@RequestBody User user) {
        return new BaseVo(userService.insert(user));
    }

    @GetMapping("/{id}/detail")
    public BaseVo getUser(@PathVariable(value = "id") String id, HttpServletRequest request) {
        // System.out.println("X-Request-Foo ================> " + request.getHeader("X-Request-Foo"));
        return new BaseVo(userService.query(id));
    }

}
