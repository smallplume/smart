package com.smt.support;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * 自定义消息生产通道
 *
 * @autor xiaoyu.fang
 * @date 2019/6/12 14:59
 */
public interface SendOutputChannel {

    String NOTE_SENDER = "noteSender";

    String EMAIL_SENDER = "emailSender";

    /**
     * 短信通道
     */
    @Output(SendOutputChannel.NOTE_SENDER)
    MessageChannel noteSender();

    /**
     * 邮件通道
     */
    @Output(SendOutputChannel.EMAIL_SENDER)
    MessageChannel emailSender();

}
