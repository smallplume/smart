package com.smt.support;

import com.smt.common.BaseVo;
import com.smt.common.ResultCode;
import com.smt.exceptions.BusinessException;
import com.smt.exceptions.UnknownException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/12 11:46
 */
@Component
@ControllerAdvice
public class ErrorHandler {

    /*@ExceptionHandler(Throwable.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public BaseVo handleAllException(Throwable ex, HttpServletRequest req, HttpServletResponse res) {
        return BaseVo.error(ResultCode.ERR_SYS_PARAM.getCode(), ResultCode.ERR_SYS_PARAM.getInfo());
    }*/

    /**
     * 绑定参数
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BindException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public BaseVo handleBindException(BindException ex) {
        return BaseVo.error(ResultCode.ERR_CHECK_PARAM.getCode(), ex.getFieldError()
                .getDefaultMessage());
    }

    /**
     * 未知异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(UnknownException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public BaseVo handleUnknownException(UnknownException ex) {
        return BaseVo.error(ex);
    }

    /**
     * 业务逻辑检验，通过异常返回
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public BaseVo handleBusinessException(BusinessException ex) {
        return BaseVo.error(ResultCode.WRONG.getCode(), ex.getMessage());
    }

    /**
     * 缺失参数
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public BaseVo handleMissingParamsException(MissingServletRequestParameterException ex) {
        return BaseVo.error(ResultCode.ERR_MISS_PARAM.getCode(), "缺失参数:" + ex.getParameterName());
    }

    /**
     * 参数验证、绑定异常
     *
     * @param ex
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public BaseVo handleValidateException(MethodArgumentNotValidException ex) {
        return BaseVo.error(ResultCode.ERR_CHECK_PARAM.getCode(), ex.getBindingResult()
                .getFieldError().getDefaultMessage());
    }

}
