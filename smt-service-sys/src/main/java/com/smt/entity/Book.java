package com.smt.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/13 10:12
 */
@Data
@Document(indexName = "bus_book", type = "book", shards = 3, replicas = 1)
public class Book implements Serializable {
    /**
     * id
     */
    @Id
    @Field(type = FieldType.Auto)
    @NotNull(message = "ID不能为空", groups = {Update.class})
    private String id;
    /**
     * 书名
     */
    @Field(type = FieldType.Text, analyzer = "ik_smart", searchAnalyzer = "ik_smart")
    @NotNull(message = "书名不能为空", groups = {Create.class, Update.class})
    private String name;
    /**
     * 作者
     */
    @Field(type = FieldType.Keyword)
    @NotNull(message = "作者不能为空", groups = {Update.class})
    private String auther;
    /**
     * 发布时间
     */
    @Field(type = FieldType.Date, format = DateFormat.basic_date, pattern = "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis")
    private Date publishTime;
    /**
     * 创建时间
     */
    @Field(type = FieldType.Date, format = DateFormat.basic_date, pattern = "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis")
    private Date createAt;

    /**
     * 新建
     */
    public interface Create {
    }

    /**
     * 更新
     */
    public interface Update {
    }

}
