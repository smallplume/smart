package com.smt.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by fxiao
 * <p>
 * indexName索引名称 可以理解为数据库名 必须为小写
 * 不然会报org.elasticsearch.indices.InvalidIndexNameException异常
 * type类型 可以理解为表名
 * on 2019/4/24
 */
@Data
@Document(indexName = "sys_user", type = "user")
public class User implements Serializable {
    /**
     * ID
     */
    @Id
    @Field(type = FieldType.Auto)
    private String id;
    /**
     * 姓名
     */
    @Field(type = FieldType.Text, index = false)
    private String name;
    /**
     * 生日
     */
    @Field(type = FieldType.Date, format = DateFormat.basic_date, pattern = "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis")
    private Date birthday;
    /**
     * 性别（0：未知；1：男；2：女；）
     */
    @Field(type = FieldType.Integer)
    private Integer sex;
    /**
     * 备注
     */
    @Field(type = FieldType.Keyword, analyzer = "ik_smart", searchAnalyzer = "ik_max_word")
    private String remark;

}
