package com.smt;

import com.smt.dto.config.ElasticConfigDTO;
import com.smt.dto.config.RabbitmqConfigDTO;
import com.smt.dto.config.RedisConfigDTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 注意：属性配置类的class需要添加到springboot的属性配置注解里面，
 * eg：@EnableConfigurationProperties({MysqlConfig.class})
 * <p>
 * 不添加的话，不能通过@Autowired注解，注入属性配置类，那么就需要在属性配置类上使用spring的bean注解，标记时一个bean到这里，代码已经完成了，
 * 启动consul服务器，下面在consul里面进行配置。
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties({RedisConfigDTO.class, ElasticConfigDTO.class, RabbitmqConfigDTO.class})
public class SmtServiceSysApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmtServiceSysApplication.class, args);
    }

}
