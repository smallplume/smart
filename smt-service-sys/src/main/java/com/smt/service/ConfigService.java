package com.smt.service;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/11 10:42
 */
public interface ConfigService {

    void setCache(String key, String value);

    String getCache(String key);
}
