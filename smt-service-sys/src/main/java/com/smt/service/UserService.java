package com.smt.service;


import com.smt.entity.User;

/**
 * Created by fxiao
 * on 2019/4/25
 */
public interface UserService {
    /**
     * 新增
     * @param user
     */
    User insert(User user);

    /**
     * 获取
     * @param id
     * @return
     */
    User query(String id);

}
