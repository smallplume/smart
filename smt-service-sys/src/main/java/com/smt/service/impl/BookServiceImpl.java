package com.smt.service.impl;

import com.smt.common.PageBean;
import com.smt.entity.Book;
import com.smt.exceptions.BusinessException;
import com.smt.repository.BookRepository;
import com.smt.service.BookService;
import com.smt.vo.BookQuery;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/13 10:31
 */
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public Book find(String id) {
        return bookRepository.findById(id).orElse(null);
    }

    @Override
    public Book insert(Book book) {
        book.setCreateAt(new Date());
        return bookRepository.index(book);
    }

    @Override
    public Book update(Book book) {
        Book entity = bookRepository.findById(book.getId()).orElse(null);
        if (entity == null) {
            throw new BusinessException("对象不存在");
        }
        book.setCreateAt(entity.getCreateAt());
        return bookRepository.save(book);
    }

    @Override
    public List<Book> query(String name, String auther) {
        Book book = new Book();
        book.setName(name);
        book.setAuther(auther);
        return bookRepository.queryByBook(book);
    }

    @Override
    public PageBean<Book> page(BookQuery bookQuery) {
        // Elasticsearch 分页从0开始
        Pageable pageable = PageRequest.of(bookQuery.getPageNum(), bookQuery.getPageSize());
        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
        if (bookQuery.getName() != null) {
            // 模糊查询
            nativeSearchQueryBuilder.withQuery(QueryBuilders.wildcardQuery("name", ("*" + bookQuery.getName() + "*").toLowerCase()));
        }
        if (bookQuery.getAuther() != null) {
            // 精确查询
            nativeSearchQueryBuilder.withQuery(QueryBuilders.termQuery("auther.keyword", bookQuery.getAuther()));
        }
        // Elasticsearch占用内存，查出来自个儿排序
//        if (bookQuery.getOrder() != null) {
//            // 排序
//            FieldSortBuilder sort = SortBuilders.fieldSort(bookQuery.getOrder()).order(SortOrder.DESC);
//            nativeSearchQueryBuilder.withSort(sort);
//        }
        nativeSearchQueryBuilder.withPageable(pageable);
        SearchQuery searchQuery = nativeSearchQueryBuilder.build();
        Page<Book> bookPage = bookRepository.search(searchQuery);
        PageBean pageBean = new PageBean(bookPage.getPageable().getPageNumber(), bookPage.getPageable().getPageSize(), bookPage.getTotalElements(), bookPage.getContent());
        return pageBean;
    }

}
