package com.smt.service.impl;

import com.smt.entity.User;
import com.smt.exceptions.BusinessException;
import com.smt.repository.UserRepository;
import com.smt.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by fxiao
 * on 2019/4/25
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public User insert(User user) {
        User result = userRepository.save(user);
        return result;
    }

    @Override
    public User query(String id) {
        Optional<User> optional = userRepository.findById(id);
        // return optional.orElse(null);
        return optional.get();
    }
    
}
