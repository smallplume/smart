package com.smt.service.impl;

import com.smt.service.SendMessage;
import com.smt.support.SendOutputChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/12 15:07
 */
@Slf4j
@EnableBinding(SendOutputChannel.class)
public class SendMessageImpl implements SendMessage {

    @Autowired
    private SendOutputChannel sendOutputChannel;

    @Override
    public void sendNoteStr(String str) {
        if (!sendOutputChannel.noteSender().send(MessageBuilder.withPayload(str).build())) {
            log.error("生产者消息发送失败：" + str);
        }
        log.info("[短信]生产者消息发送:" + str);
    }

    @Override
    public void sendEmailStr(String str) {
        if (!sendOutputChannel.emailSender().send(MessageBuilder.withPayload(str).build())) {
            log.error("生产者消息发送失败：" + str);
        }
        log.info("[邮件]生产者消息发送:" + str);
    }
}
