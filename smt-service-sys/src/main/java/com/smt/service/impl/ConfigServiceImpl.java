package com.smt.service.impl;

import com.smt.service.ConfigService;
import com.smt.utils.CacheUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/11 10:42
 */
@Service
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    private CacheUtil cacheUtil;

    @Override
    public void setCache(String key, String value) {
        cacheUtil.set(key, value);
    }

    @Override
    public String getCache(String key) {
        Object obj = cacheUtil.get(key);
        return obj == null ? null : obj.toString();
    }
}
