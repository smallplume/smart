package com.smt.service;

import com.smt.common.PageBean;
import com.smt.entity.Book;
import com.smt.vo.BookQuery;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/13 10:16
 */
public interface BookService {

    Book find(String id);

    Book insert(Book book);

    Book update(Book book);

    List<Book> query(String name, String auther);

    PageBean<Book> page(BookQuery bookQuery);

}
