package com.smt.service;

/**
 * 消息发送接口
 *
 * @autor xiaoyu.fang
 * @date 2019/6/12 15:05
 */
public interface SendMessage {

    /**
     * 短信发送
     * @param str
     */
    void sendNoteStr(String str);

    /**
     * 邮件发送
     * @param str
     */
    void sendEmailStr(String str);

}
