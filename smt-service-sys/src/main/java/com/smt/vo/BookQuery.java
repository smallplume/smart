package com.smt.vo;

import com.smt.common.BasePageVo;
import com.smt.entity.Book;
import lombok.Data;

/**
 * @autor xiaoyu.fang
 * @date 2019/6/14 11:16
 */
@Data
public class BookQuery extends BasePageVo<Book> {
    /**
     * 书名
     */
    private String name;
    /**
     * 作者
     */
    private String auther;

    private String order = "auther";
}
