package com.smt.controller;

import com.smt.common.BaseVo;
import com.smt.common.ResultCode;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @autor xiaoyu.fang
 * @date 2019/5/29 17:56
 */
@RestController
public class FallbackController {

    /**
     * 熔断
     *
     * @return
     */
    @RequestMapping(value = "/fallback", method = {RequestMethod.GET, RequestMethod.POST})
    public BaseVo fallback() {
        BaseVo response = new BaseVo();
        response.setCode(ResultCode.SERVICE_ERROR.getCode());
        response.setMsg(ResultCode.SERVICE_ERROR.getInfo());
        return response;
    }

}
