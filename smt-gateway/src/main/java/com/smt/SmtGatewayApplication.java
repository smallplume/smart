package com.smt;

import com.smt.dto.config.RedisConfigDTO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties({RedisConfigDTO.class})
public class SmtGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmtGatewayApplication.class, args);
    }

}
